@rem Do not use "echo off" to not affect any child calls.
@setlocal
@if not exist "%PORTABLE_APPS%" @set PORTABLE_APPS=%HOME%\Apps

@%PORTABLE_APPS%\PHP\php.exe %PORTABLE_APPS%\PHP\phpctags\phpctags %*
