@rem Do not use "echo off" to not affect any child calls.
@setlocal
@if not exist "%PORTABLE_APPS%" @set PORTABLE_APPS=%HOME%\Apps
@set GIT_INSTALL_ROOT=%PORTABLE_APPS%\Git
@set PATH=%GIT_INSTALL_ROOT%\bin;%GIT_INSTALL_ROOT%\mingw\bin;%PATH%
@if not exist "%HOME%" @set HOME=%USERPROFILE%

@curl.exe %*
