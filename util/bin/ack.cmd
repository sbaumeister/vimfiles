@rem Do not use "echo off" to not affect any child calls.
@setlocal
@if not exist "%PORTABLE_APPS%" @set PORTABLE_APPS=%HOME%\Apps

@rem Download portable Perl from: http://strawberryperl.com/releases.html
@%PORTABLE_APPS%\Perl\perl\bin\perl.exe %HOME%\vimfiles\util\bin\ack.pl %*
