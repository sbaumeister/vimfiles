;;-------------------------------------------------------------------------------
;; Vim
;;-------------------------------------------------------------------------------      
#If WinActive("ahk_class Vim")
  Backspace::Capslock
  CapsLock::Backspace
  RWin::RCtrl
  AppsKey::Alt
;;-------------------------------------------------------------------------------
;; Intellij IDEA
;;-------------------------------------------------------------------------------
#If WinActive("ahk_class SunAwtFrame") or WinActive("ahk_class SunAwtDialog")
  Backspace::Capslock
  CapsLock::Backspace
  RWin::RCtrl
  AppsKey::Alt
