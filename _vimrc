set nocompatible               " be iMproved
filetype off                   " required!

if has('win32') || has('win64')
    let vimfilespath = $HOME . '/vimfiles'
    if empty($PORTABLE_APPS)
        let $PORTABLE_APPS = $HOME . '\Apps'
    endif
    " Make sure to git and curl executables are available on the path
    let $PATH = $PORTABLE_APPS . '\Git\cmd;' . $HOME . '\vimfiles\util\bin;' . $PATH
    set rtp+=~/vimfiles/bundle/Vundle.vim
    call vundle#begin('$HOME/vimfiles/bundle') 
else
    let vimfilespath = $HOME . '/.vim'
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
endif

" let Vundle manage Vundle. required! 
"Plugin 'gmarik/Vundle.vim'
Plugin 'clausreinke/Vundle.vim'

" My Bundles here:
"==============================================================================
"------------------------------------------------------------------------------
" original repos on github
"------------------------------------------------------------------------------
"Plugin 'pydave/AsyncCommand'
Plugin 'Raimondi/delimitMate'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
"Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'SirVer/ultisnips'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-unimpaired'
Plugin 'sukima/xmledit'
Plugin 'mattn/emmet-vim'
Plugin 'othree/html5.vim'
Plugin 'ChrisYip/Better-CSS-Syntax-for-Vim'
Plugin 'pangloss/vim-javascript'
"Plugin 'majutsushi/tagbar'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-shell'
Plugin 'gregsexton/MatchTag'
Plugin 'mileszs/ack.vim'
"Plugin 'Rykka/colorv.vim'
Plugin 'kergoth/vim-hilinks'
Plugin 'Lokaltog/vim-powerline'
"Plugin 'Shougo/vimproc'
"Plugin 'Shougo/vimshell'
Plugin 'kien/ctrlp.vim'
"Plugin 'Yggdroot/indentLine'
Plugin 'duff/vim-scratch'
Plugin 'thanthese/Tortoise-Typing'
"Plugin 'techlivezheng/tagbar-phpctags'
Plugin 'henrik/rename.vim'
Plugin 'sjl/gundo.vim'
Plugin 'kana/vim-textobj-user'
Plugin 'kana/vim-textobj-entire'
Plugin 'kana/vim-textobj-line'
Plugin 'terryma/vim-expand-region'
Plugin 'PProvost/vim-ps1'
Plugin 'altercation/vim-colors-solarized'
"Plugin 'kana/vim-textobj-function'
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
"Plugin 'tpope/vim-rails.git'
"------------------------------------------------------------------------------
" vim.org/scripts repos
"------------------------------------------------------------------------------
Plugin 'bufexplorer.zip'
"Plugin 'LycosaExplorer'
"Plugin 'taglist.vim'
Plugin 'mru.vim'
"------------------------------------------------------------------------------
" non github repos
"------------------------------------------------------------------------------
"Plugin 'git://git.wincent.com/command-t.git'
"------------------------------------------------------------------------------
" Manual repos
"------------------------------------------------------------------------------
"Plugin 'bitbucket.org/mikehart/lycosaexplorer'
"==============================================================================

call vundle#end()
filetype plugin indent on     " required!
let &rtp = vimfilespath . '/personal,' . &rtp
