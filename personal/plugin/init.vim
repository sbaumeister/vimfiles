"let $VIMRC = vimfilespath . '/vimrc'

" Switch to Cygwin shell
let use_cygwin_bash = 0

" Get the latest PHP docs from: http://www.php.net/download-docs.php
" Choose .chm format and extract the 'res' folder (with 7-zip) to the
" specified path
"let php_doc_basedir = 'H:/WAMP/phpdoc'

" Uncomment to deactivate taglist
"let loaded_taglist = 1

" Set environment variable PATH
"let $PATH = 'H:\WAMP\php-5.3.5;F:\Cygwin\bin;F:\Programme\ctags;F:\Programme\jshint;' . $PATH

" Don't show warning message when windows style paths are used
let $CYGWIN = 'nodosfilewarning'

" Set PYTHONPATH for required python packages
let $PYTHONPATH = join([
\    $HOME . '\vimfiles' . '\python',
\], ';')

" Path to the 'activate_this.py' script of a virtualenv. Leave empty if no
" virtualenv should be loaded
"py3 activate_this = 'D:\\python\\venvs\\weblib\\Scripts\\activate_this.py'
py3 << EOF
activate_this = ''
if len(activate_this):
    execfile(activate_this, dict(__file__=activate_this))
EOF

" Simplify search-replace
map <F4> :%s//gc<Left><Left><Left>
map <S-F4> :%s/<c-r><c-w>//gc<Left><Left><Left>
vmap * y:execute "/".escape(@",'[]/\.*')<CR>
vmap <F4> y:execute "%s/".escape(@",'[]/')."//gc"<Left><Left><Left><Left>

" Insert new line in normal mode
nmap <CR> o<ESC>k
nmap <S-CR> O<ESC>j

" Insert whitespace in normal mode
nmap <C-Space> a<Space><ESC>h

" Omni completion with Ctrl + Space
imap <c-space> <c-x><c-o>

" Easier window navigation
nmap <C-J> <C-W>j
nmap <C-K> <C-W>k
nmap <C-H> <C-W>h
nmap <C-L> <C-W>l

" Easier tab navigation
nmap <c-tab> gt
nmap <c-s-tab> gT

" Close buffer without closing window
nmap <silent> <Leader>bd :Bclose<CR>

nmap <F5> :GundoToggle<CR>

" Run command and restore cursor position and search history 
function! s:Preserve(command)
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    execute a:command
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

" Remove trailing whitespace 
nmap <silent> <Leader>tw :call <SID>Preserve("%s/\\s\\+$//e")<CR>
" Realign buffer text 
nmap <silent> <Leader>== :call <SID>Preserve("normal gg=G")<CR>

" Show syntax highlighting groups for word under cursor
nmap <silent> <Leader>hg :call <SID>SynStack()<CR>
function! s:SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" Move text
nnoremap <A-down> :m+<CR>==
nnoremap <A-up> :m-2<CR>==
inoremap <A-down> <Esc>:m+<CR>==gi

inoremap <A-up> <Esc>:m-2<CR>==gi
vnoremap <A-down> :m'>+<CR>gv=gv
vnoremap <A-up> :m-2<CR>gv=gv

" Windows specific stuff
if has('win32') || has('win64')
    " Maximize window on startup
    "au GUIEnter * simalt ~x
    set grepprg=findstr\ /NPS
    "open explorer in the current file's directory
    nmap <silent> <Leader>ex :!start explorer %:p:h:8<CR>
    "open windows command prompt in the current file's directory
    nmap <silent> <Leader>cp :!start cmd /k cd %:p:h:8<CR>

    " Switch fullscreen mode
    "nmap <silent> <Leader>fs :Fullscreen<CR>
    " Fullscreen
    map <silent> <F12> :call libcallnr(vimfilespath . '\lib\gvimfullscreen.dll', 'ToggleFullScreen', 0)<CR>
    " Make window transparent
    map <silent> <S-F12> :call libcallnr(vimfilespath . '\lib\vimtweak.dll', 'SetAlpha', 245)<CR>
    " No transparency
    map <silent> <C-S-F12> :call libcallnr(vimfilespath . '\lib\vimtweak.dll', 'SetAlpha', 255)<CR>
endif


"------------------------------------------------------------------------------
" set up tab labels with tab number, buffer name, number of windows
"------------------------------------------------------------------------------
function! GuiTabLabel()
  let label = ''
  let bufnrlist = tabpagebuflist(v:lnum)
  " Add '+' if one of the buffers in the tab page is modified
  for bufnr in bufnrlist
    if getbufvar(bufnr, "&modified")
      let label = '+'
      break
    endif
  endfor
  " Append the tab number
  let label .= v:lnum.': '
  " Append the buffer name
  let name = bufname(bufnrlist[tabpagewinnr(v:lnum) - 1])
  if name == ''
    " give a name to no-name documents
    if &buftype=='quickfix'
      let name = '[Quickfix List]'
    else
      let name = '[No Name]'
    endif
  else
    " get only the file name
    let name = fnamemodify(name,":t")
  endif
  let label .= name
  " Append the number of windows in the tab page
  let wincount = tabpagewinnr(v:lnum, '$')
  return label . '  [' . wincount . ']'
endfunction
set guitablabel=%{GuiTabLabel()}

" Easy indent in normal and visual mode with arrow keys
nmap <silent> <C-Left> <<
nmap <silent> <C-Right> >>
vmap <silent> <Left> <gv
vmap <silent> <Right> >gv

"let solarized_contrast = "high"
"set background=dark
"colorscheme hybrid-modified
if has('gui_running')
  set background=light
  colorscheme enlightme
else
  set term=xterm
  set t_Co=256
  let &t_AB="\e[48;5;%dm"
  let &t_AF="\e[38;5;%dm"
  set background=dark
  colorscheme solarized
endif
"colorscheme earendel
"colorscheme solarized

syntax on
filetype plugin indent on

set encoding=utf-8
set fileencodings=utf-8,latin1
set fileformats=unix,dos

" Display line numbers
"set number

set hidden
set showcmd

" No tabs but 4 spaces
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set shiftround
set smarttab
set autoindent
set smartindent

" Allow deletion of auto inserted text 
set backspace=indent,eol,start

" Highlight current cursor line
"set cursorline

" Searching
set incsearch
set hlsearch
set ignorecase
set smartcase

set nowritebackup
set directory^=$TEMP
set backupdir^=$TEMP

set history=100

" Customize cursor blinking
"let &guicursor = substitute(&guicursor, 'n-v-c:', '&blinkwait2000-blinkon600-blinkoff300-', '')
set guicursor+=a:blinkwait800-blinkon600-blinkoff300 

" Folding
set foldmethod=indent
set nofoldenable
"set foldcolumn=1
"set foldminlines=3
hi! link FoldColumn Normal

" No line wrapping
set nowrap

" Set font
"set guifont=DejaVu_Sans_Mono:h11
"set guifont=Dina:h10
set guifont=Consolas:h10

" Reduce space between lines
set linespace=0

" No special highlighting of html tag values
let html_no_rendering = 1

"set guiheadroom=-100

" No toolbar
set guioptions-=T
" No menubar
"set guioptions-=m
" No scrollbar on the right
set guioptions-=r
" No vsplit scrollbars on the left
set guioptions-=L
" Non gui tabs
"set guioptions-=e

set winaltkeys=no

" Tabline mode: 0 - never, 1 - default, 2 - always
set showtabline=1

" Do not display welcome screen
set shortmess+=I

" Show command-line completion options
set wildmenu
set wildmode=longest,full

" Stop syntax highlighting after this column
set synmaxcol=300

" No bell sounds
set noerrorbells
au! GuiEnter * set vb t_vb=

" How to display special chars with :set list!
set listchars=tab:»·,trail:·,eol:¶

" Show status line always
set laststatus=2
" Display file encoding (and more) in the status line
"set statusline=%<%f\ %h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P
"set statusline=%f\ %m%r%y\ %{&ff},%{&fenc==\"\"?&enc:&fenc}%=%{SyntasticStatuslineFlag()}\ %-15.(Ln:%l(%L)%)\ %-10.(Col:%c%V%)\ %P

" Show no docs
set completeopt-=preview

" Go up the directory tree until a tag file is found
set tags=./tags;/

let netrw_banner = 0

" Change to user home dir on startup
au VimEnter * cd ~

" Enable PHP + HTML features for *.phtml files
au BufNewFile,BufRead *.phtml set ft=php.html

" Normal behavior of <CR> in quickfix window
au FileType qf nmap <buffer> <CR> :call <SID>NormalEnter()<CR>
function! s:NormalEnter()
    execute "normal! \<CR>"
endfunction

" Activate advanced matching of (jumping to) pairs 
source $VIMRUNTIME/macros/matchit.vim

set lines=70 columns=220

"------------------------------------------------------------------------------
" ColorV
"------------------------------------------------------------------------------
let colorv_no_global_map = 1

"------------------------------------------------------------------------------
" Powerline
"------------------------------------------------------------------------------
let Powerline_theme = "default"
let Powerline_colorscheme = "default"

"------------------------------------------------------------------------------
" UltiSnips
"------------------------------------------------------------------------------
let UltiSnipsSnippetsDir = $HOME . '\vimfiles\personal\UltiSnips'

"------------------------------------------------------------------------------
" CtrlP
"------------------------------------------------------------------------------
let ctrlp_map = '<c-q>'

"------------------------------------------------------------------------------
" NERDTree
"------------------------------------------------------------------------------
let NERDTreeChDirMode = 2
let NERDTreeDirArrows = 1
"let NERDTreeShowBookmarks=1
let NERDTreeIgnore = ['\.pyc$', '\.pyo$', '\.swp$', '\~$']
" Toggle NERDTree easily
map <silent> <F2> :NERDTreeToggle<CR>
if has('win32') || has('win64') 
    let NERDTreeIgnore += ['\$RECYCLE\.BIN', 'System Volume Information']
endif

"------------------------------------------------------------------------------
" NERDTree
"------------------------------------------------------------------------------
"let NERDCreateDefaultMappings = 0

"------------------------------------------------------------------------------
" Taglist
"------------------------------------------------------------------------------
" Open tag list on the right side
let Tlist_Use_Right_Window = 1
" Do not update tagllist window automatically for open buffers
let Tlist_Auto_Update = 0
" Show only tags for the current buffer
"let Tlist_Show_One_File = 1
" Open tag list with [F3]
map <silent> <S-F3> :TlistToggle<CR>

"------------------------------------------------------------------------------
" delimitMate
"------------------------------------------------------------------------------
let delimitMate_tab2exit = 0
inoremap <S-Space> <C-R>=delimitMate#JumpAny("\<Space>")<CR>
let delimitMate_matchpairs = "(:),[:]"
au FileType python,ruby let b:delimitMate_matchpairs = "(:),[:],{:}"

"------------------------------------------------------------------------------
" zencoding
"------------------------------------------------------------------------------
let user_zen_settings = { 'indentation': '    ' }
let user_zen_expandabbr_key = '<c-e>'

"------------------------------------------------------------------------------
" indentGuides
"------------------------------------------------------------------------------
" Use custom colors for indent guides
let indent_guides_auto_colors = 0

"------------------------------------------------------------------------------
" LycosaExplorer
"------------------------------------------------------------------------------
"noremap <silent> ,f :LycosaFilesystemExplorerFromHere<CR>
"noremap <silent> ,b :LycosaBufferExplorer<CR>

"------------------------------------------------------------------------------
" Shell
"------------------------------------------------------------------------------
"let shell_mappings_enabled = 0
command! -nargs=1 -complete=file Cmd call xolox#shell#execute(<q-args>, 0)

"------------------------------------------------------------------------------
" Syntastic
"------------------------------------------------------------------------------
"let syntastic_disabled_filetypes = ['html', 'javascript']
let syntastic_auto_loc_list = 1
let syntastic_php_checkers = ['php']
let syntastic_javascript_checkers = ['jshint']
let syntastic_javascript_jshint_conf = $HOME . '\vimfiles\util\bin\jshintrc.json'

"------------------------------------------------------------------------------
" EasyMotion
"------------------------------------------------------------------------------
let EasyMotion_leader_key = '<Leader>m'

"------------------------------------------------------------------------------
" guifont++
"------------------------------------------------------------------------------
let guifontpp_smaller_font_map = "<kMinus>"
let guifontpp_larger_font_map = "<kPlus>"
let guifontpp_original_font_map = "<kMultiply>"

"------------------------------------------------------------------------------
" Tagbar
"------------------------------------------------------------------------------
" Open tagbar with [Shift] + [F3]
map <silent> <F3> :TagbarToggle<CR>
"let tagbar_usearrows = 1

"------------------------------------------------------------------------------
" Powerline
"------------------------------------------------------------------------------
"let Powerline_symbols = 'fancy'

