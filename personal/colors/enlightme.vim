" Maintainer:   Stephan Baumeister
" Mail:         baumichel@gmail.com
" Version:      1.0
" Last Change:  July, 2012
" Credits:      This is a modification of hornet.vim color scheme

set background=light

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "enlightme"

let s:c_white = "#ffffff"
let s:c_black = "#000000"
let s:c_grey1 = "#4a4a4a"
let s:c_grey2 = "#656565"
let s:c_grey3 = "#b0b0b0"
let s:c_grey4 = "#cacaca"
let s:c_grey5 = "#d5d5d5"
let s:c_grey6 = "#d4d4d4"
let s:c_grey7 = "#e0e0e0"
let s:c_grey8 = "#e5e5e5"
let s:c_grey9 = "#fdfdfd"
let s:c_grey10 = "#f5f5f5"
let s:c_grey11 = "#959595"
let s:c_grey12 = "#ebebeb"
let s:c_grey13 = "#cbcbcb"
let s:c_yellow1 = "#fee06b"
let s:c_yellow2 = "#fee481"
let s:c_yellow3 = "#f8ec59"
let s:c_yellow4 = "#fee481"
let s:c_yellow5 = "#fedc56"
let s:c_red1 = "#a33243"
let s:c_red2 = "#ca001f"
let s:c_red3 = "#f00000"
let s:c_red4 = "#d16c7a"
let s:c_red5 = "#CC79A7"
let s:c_red6 = "#bf001d"
let s:c_red7 = "#ff95a5"
let s:c_brown1 = "#512b1e"
let s:c_brown2 = "#844631"
let s:c_brown3 = "#a8660d"
let s:c_brown4 = "#d55e00"
let s:c_brown5 = "#f7b69d"
let s:c_blue1 = "#324263"
let s:c_blue2 = "#272fc2"
let s:c_blue3 = "#0072b2"
let s:c_blue4 = "#6a83b5"
let s:c_blue5 = "#848ea9"
let s:c_blue6 = "#aab8d5"
let s:c_blue7 = "#c7cfe2"
let s:c_blue8 = "#7e8aa2"
let s:c_purple1 = "#8495e6"
let s:c_purple2 = "#b9c2f0"
let s:c_green1 = "#558817"
let s:c_green2 = "#009000"
let s:c_green3 = "#79bf21"
let s:c_green4 = "#bae981"
let s:c_green5 = "#009e73"
let s:c_green6 = "#0f8674"
let s:c_green7 = "#0e7c6b"
let s:c_green8 = "#7fc6bc"

" General colors
exe "hi Normal					guifg=" . s:c_black . "		guibg=" . s:c_grey9 . "		gui=none"
exe "hi Cursor		 			guifg=" . s:c_white . "		guibg=" . s:c_black . "		gui=none"
exe "hi Visual					guifg=fg					guibg=" . s:c_yellow3 . "	gui=none"
exe "hi NonText					guifg=" . s:c_grey3 . "		guibg=bg					gui=none"
exe "hi LineNr					guifg=" . s:c_grey11 . "	guibg=bg					gui=none"
exe "hi StatusLine				guifg=" . s:c_black . "		guibg=" . s:c_grey3 . "		gui=bold"
exe "hi StatusLineNC			guifg=" . s:c_black . "		guibg=" . s:c_grey8 . "		gui=none"
exe "hi VertSplit				guifg=" . s:c_grey13 ."		guibg=" . s:c_grey12 . "		gui=none"
exe "hi Folded					guifg=" . s:c_blue1 . "		guibg=" . s:c_grey4 . "		gui=bold"
exe "hi Title					guifg=fg					guibg=bg					gui=bold"
exe "hi Directory				guifg=" . s:c_blue3 . "		guibg=bg					gui=bold"

" Vim >= 7.0 specific colors
exe "hi ColorColumn				guifg=fg					guibg=" . s:c_grey5 . "		gui=none"
exe "hi CursorColumn			guifg=fg					guibg=" . s:c_blue7 . "		gui=none"
exe "hi CursorLine				guifg=fg					guibg=" . s:c_blue7 . "		gui=none"
exe "hi MatchParen				guifg=" . s:c_black . "		guibg=" . s:c_green4 . "	gui=bold"

exe "hi ErrorMsg				guifg=" . s:c_white . "		guibg=" . s:c_red2 . "		gui=bold"
exe "hi FoldColumn				guifg=" . s:c_grey2 . "		guibg=bg 					gui=none"
exe "hi IncSearch				guifg=fg					guibg=" . s:c_brown5 . "	gui=none"

exe "hi ModeMsg					guifg=fg					guibg=bg	gui=bold"
exe "hi MoreMsg					guifg=" . s:c_grey1 . "		guibg=bg	gui=bold"

exe "hi Pmenu					guifg=fg					guibg=" . s:c_blue6 . "		gui=none"
exe "hi PmenuSbar				guifg=fg					guibg=" . s:c_blue4 . "		gui=none"
exe "hi PmenuSel				guifg=fg					guibg=" . s:c_yellow1 . "	gui=none"
exe "hi PmenuThumb				guifg=fg					guibg=" . s:c_blue7 . "		gui=none"

exe "hi Question				guifg=" . s:c_grey1 . "		guibg=bg					gui=bold"
exe "hi Search					guifg=fg					guibg=" . s:c_yellow2 . "	gui=none"
exe "hi SignColumn				guifg=" . s:c_grey2 . "		guibg=bg					gui=none"
exe "hi SpecialKey				guifg=" . s:c_brown2 . "	guibg=bg					gui=none"

exe "hi SpellBad				guisp=" . s:c_red2 . "		gui=undercurl"
exe "hi SpellCap				guisp=" . s:c_blue2 . "		gui=undercurl"
exe "hi SpellLocal				guisp=" . s:c_green6 . "	gui=undercurl"
exe "hi SpellRare				guisp=" . s:c_red4 . "		gui=undercurl"

exe "hi TabLine					guifg=fg					guibg=" . s:c_grey6 . "		gui=underline"
exe "hi TabLineFill				guifg=fg					guibg=" . s:c_grey6 . "		gui=underline"
exe "hi TabLineSel				guifg=fg					guibg=bg					gui=bold"

exe "hi VisualNOS				guifg=" . s:c_blue1 . "		guibg=bg					gui=bold,underline"
exe "hi WarningMsg				guifg=" . s:c_red2 . "		guibg=bg					gui=bold"
exe "hi WildMenu				guifg=fg					guibg=" . s:c_yellow4 . "	gui=bold"

exe "hi Ignore					guifg=bg					guibg=bg					gui=none"
exe "hi Comment					guifg=" . s:c_blue5 . "		guibg=bg					gui=italic"
exe "hi Constant				guifg=" . s:c_brown3 . "	guibg=bg					gui=none"
exe "hi Error					guifg=" . s:c_red6 . "		guibg=bg					gui=none"
exe "hi Identifier				guifg=" . s:c_green7 . "	guibg=bg					gui=none"
exe "hi lCursor					guifg=" . s:c_white . "		guibg=" . s:c_green3 . "	gui=none"
exe "hi PreProc					guifg=" . s:c_red1 . "		guibg=bg					gui=none"
exe "hi Special					guifg=" . s:c_brown2 . "	guibg=bg					gui=none"
exe "hi Statement				guifg=" . s:c_blue3 . "		guibg=bg					gui=bold"
exe "hi Todo					guifg=" . s:c_brown1 . "	guibg=" . s:c_yellow5 . "	gui=bold"
exe "hi Type					guifg=" . s:c_blue3 . "		guibg=bg					gui=bold"
exe "hi Underlined				guifg=" . s:c_blue2 . "		guibg=bg					gui=underline"

" HTML
exe "hi link htmlTagName Keyword"
exe "hi htmlArg					guifg=" . s:c_blue3 . "		guibg=bg					gui=none"
exe "hi htmlTag					guifg=" . s:c_brown2 . "	guibg=bg					gui=bold"
exe "hi htmlEndTag				guifg=" . s:c_brown2 . "	guibg=bg					gui=bold"
exe "hi htmlString				guifg=" . s:c_green2 . "	guibg=bg					gui=none"
exe "hi htmlSpecialChar			guifg=" . s:c_red1 . "		guibg=bg					gui=bold"
exe "hi htmlComment				guifg=" . s:c_grey2 . "		guibg=" . s:c_grey10 . "	gui=italic"
exe "hi htmlCommentPart			guifg=" . s:c_grey2 . "		guibg=" . s:c_grey10 . "	gui=italic"

" XML
exe "hi xmlTagName				guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi xmlAttrib				guifg=" . s:c_blue3 . "		guibg=bg					gui=none"
exe "hi xmlTag					guifg=" . s:c_brown2 . "	guibg=bg					gui=bold"
exe "hi xmlEndTag				guifg=" . s:c_brown2 . "	guibg=bg					gui=bold"
exe "hi xmlString				guifg=" . s:c_green2 . "	guibg=bg					gui=none"
"exe "hi htmlSpecialChar			guifg=" . s:c_red1 . "		guibg=bg					gui=bold"
exe "hi xmlComment				guifg=" . s:c_grey2 . "		guibg=" . s:c_grey10 . "	gui=italic"
exe "hi xmlCommentPart			guifg=" . s:c_grey2 . "		guibg=" . s:c_grey10 . "	gui=italic"

" Diff
exe "hi DiffAdd					guifg=fg					guibg=" . s:c_green4 . "	gui=none"
exe "hi DiffChange				guifg=fg					guibg=" . s:c_purple1 . "	gui=none"
exe "hi DiffDelete				guifg=fg					guibg=" . s:c_red7 . "		gui=none"
exe "hi DiffText				guifg=fg					guibg=" . s:c_purple2 . "	gui=bold"

" Syntax highlighting
exe "hi Boolean					guifg=" . s:c_green5 . "	guibg=bg					gui=none"
exe "hi String					guifg=" . s:c_green1 . "	guibg=bg					gui=none"
exe "hi Function				guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi Keyword					guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi Number					guifg=" . s:c_red1 . "		guibg=bg					gui=none"

" Indent guides
exe "hi IndentGuidesOdd			guifg=fg					guibg=" . s:c_grey10 . "	gui=none"
exe "hi IndentGuidesEven		guifg=fg					guibg=" . s:c_grey8 . "		gui=none"

" NERDTree
exe "hi NERDTreeCWD				guifg=" . s:c_brown3 . "	guibg=bg					gui=none"
exe "hi NERDTreeExecFile		guifg=" . s:c_red5 . "		guibg=bg					gui=bold"

" PHP
exe "hi phpDefine				guifg=" . s:c_red1 . "		guibg=bg					gui=bold"
exe "hi phpInclude				guifg=" . s:c_blue2 . "		guibg=bg					gui=bold"
exe "hi phpStructure			guifg=" . s:c_grey2 . "		guibg=bg					gui=bold"
exe "hi phpFunctions			guifg=" . s:c_brown2 . "	guibg=bg					gui=none"
exe "hi phpConditional			guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi phpRepeat				guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi phpStringSingle			guifg=" . s:c_green2 . "	guibg=bg					gui=none"
exe "hi phpStringDouble			guifg=" . s:c_green2 . "	guibg=bg					gui=none"
exe "hi phpOperator				guifg=" . s:c_red2 . "		guibg=bg					gui=bold"
exe "hi phpComparison			guifg=" . s:c_red2 . "		guibg=bg					gui=bold"
exe "hi phpSpecialFunction		guifg=" . s:c_brown3 . "	guibg=bg					gui=italic"
exe "hi phpCoreConstant			guifg=" . s:c_brown3 . "	guibg=bg					gui=italic"
exe "hi phpParent				guifg=" . s:c_black . "		guibg=bg					gui=bold"
exe "hi phpNumber				guifg=" . s:c_red3 . "		guibg=bg					gui=none"
exe "hi phpBoolean				guifg=" . s:c_red5 . "		guibg=bg					gui=bold"
exe "hi phpComment				guifg=" . s:c_grey2 . "		guibg=" . s:c_grey10 . "	gui=italic"

" JavaScript
exe "hi jsFunction				guifg=" . s:c_red1 . "		guibg=bg					gui=bold"
exe "hi jsRepeat				guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi jsBranch				guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi jsConditional			guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi jsStatement				guifg=" . s:c_brown4 . "	guibg=bg					gui=bold"
exe "hi jsThis					guifg=" . s:c_blue2 . "		guibg=bg					gui=italic"
exe "hi jsComment				guifg=" . s:c_grey2 . "		guibg=" . s:c_grey10 . "	gui=italic"
exe "hi jsNumber				guifg=" . s:c_red3 . "		guibg=bg					gui=none"
exe "hi jsType					guifg=" . s:c_red6 . "		guibg=bg					gui=bold"
exe "hi jsOperator				guifg=" . s:c_green7 . "	guibg=bg					gui=bold"
exe "hi jsBoolean				guifg=" . s:c_red6 . "		guibg=bg					gui=bold"
exe "hi jsNull					guifg=" . s:c_red6 . "		guibg=bg					gui=bold"
exe "hi jsReturn				guifg=" . s:c_blue1 . "		guibg=bg					gui=bold"
exe "hi jsStringS				guifg=" . s:c_green2 . "	guibg=bg					gui=none"
exe "hi jsStringD				guifg=" . s:c_green2 . "	guibg=bg					gui=none"

" Python
"exe "hi pythonImport			guifg=" . s:c_green8 . "	guibg=bg					gui=none"
"exe "hi pythonException		guifg=" . s:c_red3 . "		guibg=bg					gui=none"
"exe "hi pythonOperator			guifg=" . s:c_blue8 . "		guibg=bg					gui=none"
"exe "hi pythonBuiltinFunction	guifg=" . s:c_green2 . "	guibg=bg					gui=none"
"exe "hi pythonExClass			guifg=" . s:c_green2 . "	guibg=bg					gui=none"

