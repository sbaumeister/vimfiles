" Maintainer:   Stephan Baumeister
" Mail:         baumichel@gmail.com
" Version:      1.0
" Last Change:  July, 2012
" Credits:      This is a modification of hornet.vim color scheme

set background=light

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "enlightme"

" General colors
hi Normal 		   guifg=#000000 guibg=#FDFDFD gui=none
hi Cursor 		   guifg=#FFFFFF guibg=#000000 gui=none
hi Visual		   guibg=#f8ec59 gui=none
hi NonText 		   guifg=#656565 guibg=bg gui=none
hi LineNr 		   guifg=#656565 guibg=bg gui=none
hi StatusLine 	   guifg=#000000 guibg=#b0b0b0 gui=bold
hi StatusLineNC    guifg=#000000 guibg=#e5e5e5 gui=none
hi VertSplit 	   guifg=#000000 guibg=#e5e5e5 gui=none
hi Folded 		   guifg=#324263 guibg=#cacaca gui=bold
hi Title		   gui=bold
hi Directory 	   guifg=#0072b2 guibg=bg gui=bold 

" Vim >= 7.0 specific colors
hi ColorColumn   guibg=#d5d5d5 gui=none
hi CursorColumn  guibg=#e0e0e0 gui=none
hi CursorLine    guibg=#e0e0e0 gui=none
hi MatchParen    guifg=#FFFFFF guibg=#0f8674 gui=bold

hi ErrorMsg guibg=#ca001f guifg=#ffffff gui=bold
hi FoldColumn guibg=bg guifg=#656565 gui=none
hi IncSearch guibg=#f7b69d gui=none

hi ModeMsg guibg=bg guifg=fg gui=bold
hi MoreMsg guibg=bg guifg=#4a4a4a gui=bold

hi Pmenu guibg=#aab8d5 guifg=fg gui=none
hi PmenuSbar guibg=#6a83b5 guifg=fg gui=none
hi PmenuSel guibg=#fee06b guifg=fg gui=none
hi PmenuThumb guibg=#c7cfe2 guifg=fg gui=none

hi Question guibg=bg guifg=#4a4a4a gui=bold
hi Search guibg=#fee481 gui=none
hi SignColumn guibg=bg guifg=#656565 gui=none
hi SpecialKey guibg=bg guifg=#844631 gui=none

hi SpellBad guisp=#ca001f gui=undercurl
hi SpellCap guisp=#272fc2 gui=undercurl
hi SpellLocal guisp=#0f8674 gui=undercurl
hi SpellRare guisp=#d16c7a gui=undercurl

hi TabLine guibg=#d4d4d4 guifg=fg gui=underline
hi TabLineFill guibg=#d4d4d4 guifg=fg gui=underline
hi TabLineSel guibg=bg guifg=fg gui=bold

hi VisualNOS guibg=bg guifg=#324263 gui=bold,underline
hi WarningMsg guibg=bg guifg=#ca001f gui=bold
hi WildMenu guibg=#fedc56 guifg=fg gui=bold

hi Comment guibg=bg guifg=#848ea9 gui=italic
hi Constant guibg=bg guifg=#a8660d gui=none
hi Error guibg=bg guifg=#bf001d gui=none
hi Identifier guibg=bg guifg=#0e7c6b gui=none
hi Ignore guibg=bg guifg=bg gui=none
hi lCursor guibg=#79bf21 guifg=#ffffff gui=none
hi PreProc guibg=bg guifg=#a33243 gui=none
hi Special guibg=bg guifg=#844631 gui=none
hi Statement guibg=bg guifg=#0072b2 gui=bold
hi Todo guibg=#fedc56 guifg=#512b1e gui=bold
hi Type guibg=bg guifg=#0072b2 gui=bold
hi Underlined guibg=bg guifg=#272fc2 gui=underline

" HTML
hi link htmlTag Identifier 
hi link htmlTagName Keyword 
hi htmlBold guibg=bg guifg=fg gui=bold
hi htmlBoldItalic guibg=bg guifg=fg gui=bold,italic
hi htmlBoldUnderline guibg=bg guifg=fg gui=bold,underline
hi htmlBoldUnderlineItalic guibg=bg guifg=fg gui=bold,underline,italic
hi htmlItalic guibg=bg guifg=fg gui=italic
hi htmlUnderline guibg=bg guifg=fg gui=underline
hi htmlUnderlineItalic guibg=bg guifg=fg gui=underline,italic

" Diff
hi DiffAdd guibg=#bae981 guifg=fg gui=none
hi DiffChange guibg=#8495e6 guifg=fg gui=none
hi DiffDelete guibg=#ff95a5 guifg=fg gui=none
hi DiffText guibg=#b9c2f0 guifg=fg gui=bold

" Syntax highlighting
hi Boolean         guifg=#009e73 gui=none
"hi String 		   guifg=#a33243 gui=none 
hi String 		   guifg=#558817 gui=none 
hi Function 	   guifg=#d55e00 gui=bold
hi Keyword		   guifg=#d55e00 gui=bold
hi Number		   guifg=#a33243 gui=none
"hi Number		   guifg=#009e73 gui=none

" Indent guides
hi IndentGuidesOdd  guibg=#F5F5F5
hi IndentGuidesEven guibg=#E5E5E5

" NERDTree
hi NERDTreeCWD guifg=#a8660d gui=none
hi NERDTreeExecFile guifg=#CC79A7 gui=bold

" Python
"hi pythonImport          guifg=#7fc6bc gui=none
"hi pythonException       guifg=#f00000 gui=none
"hi pythonOperator        guifg=#7e8aa2 gui=none
"hi pythonBuiltinFunction guifg=#009000 gui=none
"hi pythonExClass         guifg=#009000 gui=none

