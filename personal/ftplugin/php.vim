" Based on: php_doc.vim (http://www.vim.org/scripts/script.php?script_id=2435)

if exists("b:loaded_php_personal")
    finish
endif
let b:loaded_php_personal = 1

if !exists("s:php_doc_func")
python << EOF
import vim
import webbrowser
def open_php_doc():
    funcname = vim.eval('expand("<cword>")')
    use_local_doc = vim.eval('exists("php_doc_basedir")')
    if use_local_doc:
        local_doc = vim.eval('php_doc_basedir')
        url = 'file:///' + local_doc + '/function.' + funcname.replace('_', '-') + '.html'
    else:
        url = "http://php.net/" + funcname
    webbrowser.open_new_tab(url)
    return None
EOF
let s:php_doc_func = 1
endif

noremap <silent><buffer> <F1> :py open_php_doc()<CR>
let b:undo_ftplugin = "mapclear <buffer>"
